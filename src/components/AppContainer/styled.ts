import styled, { css } from 'styled-components';

export const ContainerApp = styled.div`
  width: 100%;
  height: auto;
  clear: both;
  float: left;
  position: relative;
`;

export const Main = styled.div`
  ${({ theme }) => css`
    background-color: ${theme.colors.primaryColor};
    width: 100%;
    min-height: 100vh;
    float: left;
    position: relative;
    padding-left: 330px;
    @media (max-width: 980px) {
      padding-left: 0;
    }
  `}
`;

export const Content = styled.div`
  ${({ theme }) => css`
    position: relative;
    width: 100%;
    float: left;
    clear: both;
    border-left: 1px solid #ebebeb;
    border-left-color: ${theme.colors.primaryColor};
    min-height: 100vh;
  `}
`;

export const ContentFixed = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  margin-left: 330px;
  overflow-x: hidden;
  overflow-y: auto;
  z-index: 99;
  @media (max-width: 980px) {
    margin-left: 0;
  }
`;

export const Container = styled.div`
  height: 100%;
  max-width: 960px;
  width: 100%;
  margin-left: auto;
  margin-right: auto;
  height: auto;
  padding: 0 40px;
  position: relative;
  clear: both;
`;

export const MenuLateral = styled.div`
  ${({ theme }) => css`
    background-color: ${theme.colors.secondaryColor};
    width: 330px;
    height: 100vh;
    position: fixed;
    left: 0;
    top: 0;
    display: flex;
    align-items: center;
    z-index: 10;
    padding: 0 100px;

    @media (max-width: 980px) {
      position: fixed;
      width: 100%;
      height: auto;
      padding: 0;
      top: 0;
      left: 0;
      right: 0;
      z-index: 999;
    }
  `}
`;
