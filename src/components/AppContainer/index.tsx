import React, { ReactNode } from 'react';
import { NavLateral } from '../NavLateral';
import {
  ContainerApp,
  MenuLateral,
  Main,
  Content,
  ContentFixed,
  Container,
} from './styled';

type ContainerAppProps = {
  children: ReactNode;
};

export const AppContainer: React.FC<ContainerAppProps> = ({
  children,
}: ContainerAppProps) => {
  return (
    <>
      <ContainerApp>
        <MenuLateral>
          <NavLateral />
        </MenuLateral>
        <Main>
          <Content>
            <ContentFixed>
              <Container>{children}</Container>
            </ContentFixed>
          </Content>
        </Main>
      </ContainerApp>
    </>
  );
};
