import styled, { css } from 'styled-components';

export const Container = styled.div`
  ${({ theme }) => css`
    width: 100%;
    height: auto;
    clear: both;
    float: left;
    margin: 0 0 50px 25px;
    div {
      width: 100%;
      height: auto;
      clear: both;
      display: flex;
      justify-content: space-between;
      align-items: flex-end;
      div {
        h2 {
          color: ${theme.colors.white};
          font-weight: 800;
          font-size: ${theme.font.sizes.xxlarge};
        }
      }
    }
  `}
`;
