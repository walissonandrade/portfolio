import { ReactElement } from 'react';
import { Container } from './styled';

type TituloProps = {
  titulo: string;
};

export const Titulo = ({ titulo }: TituloProps): ReactElement => {
  return (
    <Container>
      <div>
        <div>
          <h2>{titulo}</h2>
        </div>
      </div>
    </Container>
  );
};
