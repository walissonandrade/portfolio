import { ReactElement } from 'react';
import { ListaContato } from '../ListaContato';
import { Container, Avatar, DivImage, Detalhes } from './styled';

export const Sobre = (): ReactElement => {
  return (
    <Container>
      <Avatar>
        <DivImage />
      </Avatar>
      <Detalhes>
        <h1>Walisson Andrade</h1>
        <p>
          Sou formado em ciências da computação na{' '}
          <b> Faculdade Pitágoras de Betim-mg.</b>
          <br />
          Minha especialidade é <b className="animacao" />
        </p>
        <ListaContato />
      </Detalhes>
    </Container>
  );
};
