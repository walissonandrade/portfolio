import styled, { css } from 'styled-components';
import walisson from '../../assets/img/walisson.png';

export const Container = styled.div`
  ${({ theme }) => css`
    display: flex;
    align-items: center;
    color: ${theme.colors.white};

    @media (max-width: 980px) {
      flex-direction: column;
      text-align: center;
    }
  `}
`;

export const Avatar = styled.div`
  min-width: 300px;
  min-height: 300px;
  position: relative;
  border-radius: 100%;

  @media (max-width: 980px) {
    margin: 20px;
    min-width: 200px;
    min-height: 200px;
  }
  @media (max-width: 320px) {
    margin: 100px 0 20px 0;
    min-width: 200px;
    min-height: 200px;
  }
`;

export const DivImage = styled.div`
  background-image: url(${walisson});
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background-repeat: no-repeat;
  background-position: 50%;
  background-size: cover;
  animation: morph 7s ease-in-out 1s infinite;
  background-blend-mode: multiply;
  box-shadow: inset 0 0 0 9px hsl(0deg 0% 100% / 25%);
  border-radius: 70% 38% 28% 68%/60% 30% 70% 38%;
  overflow: hidden;

  @keyframes morph {
    0% {
      border-radius: 70% 38% 28% 68%/60% 30% 70% 38%;
    }
    50% {
      border-radius: 28% 58% 70% 40%/50% 60% 30% 60%;
    }
    to {
      border-radius: 70% 38% 28% 68%/60% 30% 70% 38%;
    }
  }
`;

export const Detalhes = styled.div`
  ${({ theme }) => css`
    margin-left: 80px;
    @media (max-width: 980px) {
      margin-left: 0;
    }
    h1 {
      font-size: ${theme.font.sizes.xxxlarge};
      font-weight: 800;
      text-transform: uppercase;
      margin-bottom: 19px;
    }
    p {
      font-style: italic;
    }
    p::after {
      content: '';
      color: ${theme.colors.gray};
      font-weight: bold;
      animation: profile 8s infinite;
      border-bottom: 1px solid;
    }
    @keyframes profile {
      0% {
        content: 'desenvolvimento front-end.';
      }
      20% {
        content: 'desenvolvimento front-end.';
      }
      40% {
        content: 'javaScript.';
      }
      60% {
        content: 'typeScript.';
      }
      80% {
        content: 'react.';
      }
      100% {
        content: 'vue.';
      }
    }
  `}
`;
