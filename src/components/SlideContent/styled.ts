import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  min-height: 100vh;
  clear: both;
  justify-content: center;
  position: relative;
  animation: customOne 0.8s ease-in-out 0s 1 normal none running;
  @keyframes customOne {
    0% {
      opacity: 0;
      transform: translateX(50px);
    }
    to {
      opacity: 1;
      transform: none;
    }
  }
  @media (max-width: 980px) {
    flex-direction: column;
    align-items: center;
    margin-top: 70px;
  }
`;
