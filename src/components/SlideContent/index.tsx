import { ReactElement, ReactNode } from 'react';
import { Container } from './styled';

type SlideContentProps = {
  children: ReactNode;
};

export const SlideContent = ({ children }: SlideContentProps): ReactElement => {
  return <Container>{children}</Container>;
};
