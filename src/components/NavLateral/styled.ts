import { Link } from 'react-router-dom';
import styled, { css } from 'styled-components';
import { IsActiveProp } from '.';

export const Container = styled.div`
  width: 100%;
  height: auto;
`;

export const Menu = styled.div`
  padding: 40px 0 50px 0;
  width: 100%;
  float: left;
  margin: 0;
  list-style-type: none;

  @media (max-width: 980px) {
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 20px 40px;
    height: auto;
  }
`;

export const LinkItem = styled(Link)<IsActiveProp>`
  ${({ theme, active }) => css`
    margin: 0;
    width: 100%;
    float: left;
    display: inline-block;
    cursor: pointer;
    font-weight: bold;
    line-height: 2em;
    font-size: ${theme.font.sizes.xmedium};
    color: ${active === 1 ? theme.colors.white : theme.colors.gray};
    transition: all 0.3s ease;
    :hover {
      color: ${theme.colors.white};
    }

    @media (max-width: 980px) {
      display: none;
    }
  `}
`;

export const LinkItemIcon = styled(Link)<IsActiveProp>`
  ${({ theme, active }) => css`
    display: none;
    margin: 0;
    width: 100%;
    float: left;
    cursor: pointer;
    font-weight: bold;
    line-height: 2em;
    color: ${active === 1 ? theme.colors.white : theme.colors.gray};
    text-align: center;
    transition: all 0.3s ease;
    :hover {
      color: ${theme.colors.white};
    }
    @media (max-width: 980px) {
      display: inline-block;
    }
  `}
`;
