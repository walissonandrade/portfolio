import { ReactElement } from 'react';
import { useHistory } from 'react-router-dom';
import { FaBitbucket, FaHome, FaUniversity } from 'react-icons/fa';
import * as Styled from './styled';

export type IsActiveProp = {
  active: number;
};

export const NavLateral = (): ReactElement => {
  const { location } = useHistory();

  return (
    <Styled.Container>
      <Styled.Menu>
        {/* Home */}
        <Styled.LinkItem to="/" active={location.pathname === '/' ? 1 : 0}>
          Home
        </Styled.LinkItem>
        <Styled.LinkItemIcon to="/" active={location.pathname === '/' ? 1 : 0}>
          <FaHome size={21} />
        </Styled.LinkItemIcon>
        {/* Conhecimentos */}
        <Styled.LinkItemIcon
          active={location.pathname === '/conhecimentos' ? 1 : 0}
          to="/conhecimentos"
        >
          <FaUniversity size={21} />
        </Styled.LinkItemIcon>
        <Styled.LinkItem
          active={location.pathname === '/conhecimentos' ? 1 : 0}
          to="/conhecimentos"
        >
          Conhecimentos
        </Styled.LinkItem>
        {/* Repositório */}
        <Styled.LinkItemIcon
          active={location.pathname === '/repositorio' ? 1 : 0}
          to="/repositorio"
        >
          <FaBitbucket size={21} />
        </Styled.LinkItemIcon>
        <Styled.LinkItem
          active={location.pathname === '/repositorio' ? 1 : 0}
          to="/repositorio"
        >
          Repositório
        </Styled.LinkItem>
      </Styled.Menu>
    </Styled.Container>
  );
};
