import styled, { css } from 'styled-components';

export const Container = styled.ul`
  ${({ theme }) => css`
    width: 100%;
    float: left;
    padding: 0;
    li {
      margin: 0 15px 0 0;
      display: inline-block;
      a {
        color: ${theme.colors.white};
        :hover {
          color: ${theme.colors.gray};
        }
      }
    }
  `}
`;
