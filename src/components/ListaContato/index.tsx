import { ReactElement } from 'react';
import linksFooter from '../../mock/linksFooter';
import { Container } from './styled';

export const ListaContato = (): ReactElement => {
  return (
    <Container>
      {linksFooter.map(contato => (
        <li key={contato.urlLink}>
          <a
            href={contato.urlLink}
            target="_blank"
            title={contato.nome}
            rel="noreferrer"
          >
            <contato.icon size={27} />
          </a>
        </li>
      ))}
    </Container>
  );
};
