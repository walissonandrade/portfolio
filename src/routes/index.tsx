import { ReactElement } from 'react';
import { Switch } from 'react-router-dom';
import { Home } from '../pages/Home';
import { Conhecimentos } from '../pages/Conhecimentos';
import { Repositorio } from '../pages/Repositorio';
import { NotFound } from '../pages/NotFound';
import AppRoute from './appRoute';

export const Routes = (): ReactElement => {
  return (
    <Switch>
      <AppRoute component={Home} path="/" exact />
      <AppRoute component={Conhecimentos} path="/conhecimentos" exact />
      <AppRoute component={Repositorio} path="/repositorio" exact />
      <AppRoute component={NotFound} path="*" />
    </Switch>
  );
};
