import { FunctionComponent, ReactElement } from 'react';
import { Route } from 'react-router-dom';
import { AppContainer } from '../components/AppContainer';

type AppRouteProps = {
  component: FunctionComponent;
  path: string;
  exact?: boolean;
};

const AppRoute = ({
  component,
  path,
  exact = false,
}: AppRouteProps): ReactElement => {
  return (
    <AppContainer>
      <Route component={component} path={path} exact={exact} />
    </AppContainer>
  );
};

export default AppRoute;
