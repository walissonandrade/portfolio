import { ReactElement } from 'react';
import { Row, Col } from 'react-bootstrap';
import { SlideContent } from '../../components/SlideContent';
import { Titulo } from '../../components/Titulo';
import habilidades from '../../mock/habilidades';
import { Container, Content, Detalhe } from './styled';

export const Conhecimentos = (): ReactElement => {
  return (
    <SlideContent>
      <Container>
        <Titulo titulo="Conhecimentos" />
        <Row>
          {habilidades.map(habilidade => (
            <Col xs={12} sm={6} md={4} key={habilidade.name}>
              <Content image={habilidade.image}>
                <div>
                  <img src={habilidade.image} alt={habilidade.name} />
                  <div className="image" />
                </div>
                <Detalhe>
                  <div className="paragrafo">
                    <p>{habilidade.description}</p>
                  </div>
                  <h3>{habilidade.name}</h3>
                </Detalhe>
              </Content>
            </Col>
          ))}
        </Row>
      </Container>
    </SlideContent>
  );
};
