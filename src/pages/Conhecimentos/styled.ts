import styled, { css } from 'styled-components';

type ImageProp = {
  image: string;
};

export const Container = styled.div`
  ${({ theme }) => css`
    color: ${theme.colors.white};
    margin-top: 2.5em;
  `}
`;

export const Content = styled.div<ImageProp>`
  ${({ theme, image }) => css`
    background: ${theme.colors.secondaryColor};
    margin-bottom: 30px;
    box-shadow: 0 0 20px ${theme.colors.secondaryColor};
    width: 100%;
    height: auto;
    clear: both;
    float: left;
    position: relative;
    background: #fff;
    div {
      position: relative;
      overflow: hidden;
      background: ${theme.colors.secondaryColor};
      img {
        min-width: 100%;
        opacity: 0;
        max-width: 100%;
        vertical-align: middle;
        transition: all 0.3s;
      }
      .image {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        background-image: url(${image});
        background-repeat: no-repeat;
        background-size: cover;
        background-position: 50%;
        transition: all 0.3s ease-in-out;
        filter: grayscale(75%);
        :hover {
          transform: scale(1.2);
          filter: grayscale(30%);
        }
      }
    }
  `}
`;

export const Detalhe = styled.div`
  ${({ theme }) => css`
    width: 100%;
    float: left;
    padding: 20px;
    padding-top: 32px;
    .paragrafo {
      display: flex;
      align-items: center;
      justify-content: space-between;
      margin-bottom: 15px;
      position: relative;
      border-bottom: 1px solid rgba(0, 0, 0, 0.1);
      padding-bottom: 5px;
      p {
        text-transform: capitalize;
        font-size: ${theme.font.sizes.small};
        color: ${theme.colors.gray};
        font-style: italic;
      }
    }
    h3 {
      position: relative;
      margin-bottom: 10px;
      color: #fff;
      font-size: 18px;
      font-weight: 600;
      line-height: 1.4;
      text-align: center;
    }
  `}
`;
