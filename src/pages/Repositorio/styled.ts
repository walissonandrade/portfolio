import styled, { css } from 'styled-components';

type ImageProp = {
  image: string;
};

export const Container = styled.div`
  ${({ theme }) => css`
    color: ${theme.colors.white};
    margin-top: 2.5em;
  `}
`;

export const Content = styled.div<ImageProp>`
  ${({ theme, image }) => css`
    background: ${theme.colors.secondaryColor};
    box-shadow: 0 0 20px ${theme.colors.secondaryColor};
    width: 100%;
    height: 100%;
    clear: both;
    float: left;
    position: relative;
    transition: all 0.3s ease-in-out;
    div {
      position: relative;
      overflow: hidden;
      background: ${theme.colors.secondaryColor};
      img {
        min-width: 100%;
        opacity: 0;
        max-width: 100%;
        vertical-align: middle;
        transition: all 0.3s;
      }
      .image {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        background-image: url(${image});
        background-repeat: no-repeat;
        background-size: cover;
        background-position: 50%;
        transition: all 0.3s ease-in-out;
        filter: grayscale(75%);
        :hover {
          transform: scale(1.2);
          filter: grayscale(50%);
        }
      }
    }
  `}
`;

export const Detalhe = styled.div`
  ${({ theme }) => css`
    width: 100%;
    float: left;
    padding: 10px;
    padding-top: 32px;
    .paragrafo {
      display: flex;
      align-items: center;
      justify-content: space-between;
      margin-bottom: 15px;
      position: relative;
      border-bottom: 1px solid rgba(0, 0, 0, 0.1);
      padding-bottom: 5px;
      p {
        text-transform: capitalize;
        font-family: Lato;
        font-size: 12px;
        color: ${theme.colors.gray};
        font-style: italic;
        margin-top: 20px;
        a {
          font-style: normal;
          color: ${theme.colors.white};
          :hover {
            font-weight: bold;
          }
        }
      }
    }
    h3 {
      position: relative;
      color: ${theme.colors.white};
      font-size: 14px;
      font-weight: 600;
      line-height: 1.4;
      text-align: center;
    }
  `}
`;

export const NextPage = styled.div`
  ${({ theme }) => css`
    text-align: center;
    button {
      margin: 20px 0 50px 0;
      background: transparent;
      border: none;
      color: ${theme.colors.white};
      transition: all 0.3s ease-in-out;
      :hover {
        color: ${theme.colors.gray};
      }
    }
  `}
`;
