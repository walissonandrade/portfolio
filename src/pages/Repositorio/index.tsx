import { ReactElement, useEffect, useState } from 'react';
import { Row, Col } from 'react-bootstrap';
import { FaArrowCircleDown } from 'react-icons/fa';
import { SlideContent } from '../../components/SlideContent';
import { Titulo } from '../../components/Titulo';
import { HttpService } from '../../service';
import { Container, Detalhe, Content, NextPage } from './styled';

type LinksProps = {
  avatar: {
    href: string;
  };
  html: {
    href: string;
  };
};

export type RepositorioProps = {
  name: string;
  full_name: string;
  language: string;
  created_on: string;
  links: LinksProps;
};

export type PaginationProps = {
  next: string;
  page: number;
  pagelen: number;
  size: number;
};

export const Repositorio = (): ReactElement => {
  const [repositorios, setRepositorios] = useState<RepositorioProps[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [pagination, setPagination] = useState<PaginationProps>();

  const getRepositorio = async (pagina: number) => {
    const service = new HttpService();
    try {
      const values = await service.get(pagina);
      const dadosPage: PaginationProps = values?.data;
      if (dadosPage) {
        setPagination(dadosPage);
      }
      const dados: RepositorioProps[] = values?.data?.values;
      const dadosFormatado = dados.map(item => {
        const dt = new Date(item.created_on);
        const dataFormatada = dt.toLocaleDateString('pt-BR', {
          timeZone: 'UTC',
        });
        return {
          ...item,
          created_on: dataFormatada,
        };
      });
      setRepositorios([...repositorios, ...dadosFormatado]);
      setLoading(false);
    } catch (e) {
      // eslint-disable-next-line no-alert
      alert(e);
      setLoading(false);
    }
  };

  const newPage = () => {
    if (pagination?.next && pagination?.next !== '') {
      getRepositorio(pagination.page + 1);
      setPagination({
        ...pagination,
        page: pagination.page + 1,
      });
    }
  };

  useEffect(() => {
    getRepositorio(1);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (loading) {
    return <h1>Carregando...</h1>;
  }
  return (
    <SlideContent>
      <Container>
        <Titulo titulo="Repositório" />
        <Row>
          {repositorios.map(rep => (
            <Col
              xs={12}
              sm={6}
              md={3}
              key={rep.full_name + rep.created_on + rep.name}
              style={{ marginBottom: '30px' }}
            >
              <Content image={rep.links.avatar.href}>
                <div>
                  <img src={rep.links.avatar.href} alt={rep.name} />
                  <div className="image" />
                </div>
                <Detalhe>
                  <h3>{rep.name}</h3>
                  <div className="paragrafo">
                    <p>
                      {rep.created_on} <br />
                      {rep.language} <br />
                      <a
                        target="_blank"
                        href={rep.links.html.href}
                        rel="noreferrer"
                      >
                        Bitbucket
                      </a>
                    </p>
                  </div>
                </Detalhe>
              </Content>
            </Col>
          ))}
        </Row>
        {pagination?.next && (
          <Row className="justify-content-md-center">
            <Col md={4}>
              <NextPage>
                <button type="button" onClick={() => newPage()}>
                  <FaArrowCircleDown size={33} />
                </button>
              </NextPage>
            </Col>
          </Row>
        )}
      </Container>
    </SlideContent>
  );
};
