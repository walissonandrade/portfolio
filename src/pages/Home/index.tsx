import { ReactElement } from 'react';
import { SlideContent } from '../../components/SlideContent';
import { Sobre } from '../../components/Sobre';
import { Container } from './styled';

export const Home = (): ReactElement => {
  return (
    <Container>
      <SlideContent>
        <Sobre />
      </SlideContent>
    </Container>
  );
};
