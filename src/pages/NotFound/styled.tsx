import styled, { css } from 'styled-components';

export const Container = styled.div`
  ${({ theme }) => css`
    border-top: 1px solid ${theme.colors.thirdaryColor};
    border-bottom: 1px solid ${theme.colors.thirdaryColor};
    padding: 8em 1.5em 5em 1.5em;
  `}
`;
