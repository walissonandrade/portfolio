import { ReactElement } from 'react';

import { SlideContent } from '../../components/SlideContent';
import { Titulo } from '../../components/Titulo';
import { Container } from './styled';

export const NotFound = (): ReactElement => {
  return (
    <SlideContent>
      <Container>
        <Titulo titulo="Counteúdo Não Encontrado" />
      </Container>
    </SlideContent>
  );
};
