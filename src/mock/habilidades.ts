import js from '../assets/img/js.png';
import css from '../assets/img/css.png';
import html from '../assets/img/html.png';
import ts from '../assets/img/ts.png';
import react from '../assets/img/react.png';
import vue from '../assets/img/vue.png';

export default [
  {
    name: 'HTML',
    description: 'Linguagem de marcação',
    image: html,
  },
  {
    name: 'CSS',
    description: 'Folha de estilo',
    image: css,
  },
  {
    name: 'JavaScript',
    description: 'Linguagem de programação',
    image: js,
  },
  {
    name: 'TypeScript',
    description: 'Linguagem de programação',
    image: ts,
  },
  {
    name: 'React',
    description: 'Framework front-end',
    image: react,
  },
  {
    name: 'Vue',
    description: 'Framework front-end',
    image: vue,
  },
];
