import { FaLinkedin, FaInstagramSquare, FaBitbucket } from 'react-icons/fa';

export default [
  {
    nome: 'Linkedin',
    icon: FaLinkedin,
    urlLink: 'https://www.linkedin.com/in/walisson-andrade-900009a1/',
  },
  {
    nome: 'Instagram',
    icon: FaInstagramSquare,
    urlLink: 'https://www.instagram.com/walissonra/?hl=pt-br',
  },
  {
    nome: 'Bitbucket',
    icon: FaBitbucket,
    urlLink: 'https://bitbucket.org/walissonandrade/',
  },
];
