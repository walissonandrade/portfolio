import axios, { AxiosPromise } from 'axios';

export class HttpService {
  readonly service = axios.create({
    headers: {
      'Content-Type': 'application/json',
    },
  });

  readonly url: string =
    'https://bitbucket.org/api/2.0/repositories/walissonandrade';

  get(page: number): AxiosPromise {
    return this.service.get(`${this.url}?page=${page}`);
  }
}
