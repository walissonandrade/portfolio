export const theme = {
  colors: {
    primaryColor: '#231f1f',
    secondaryColor: '#100f0f',
    thirdaryColor: '#982b53',
    gray: '#bbbbbb',
    white: '#FFFFFF',
    black: '#000000',
  },
  font: {
    family: {
      default: 'Lato, sans-serif',
    },
    sizes: {
      small: '13px',
      medium: '16px',
      xmedium: '20px',
      large: '24px',
      xlarge: '28px',
      xxlarge: '34px',
      xxxlarge: '42px',
    },
  },
} as const;
