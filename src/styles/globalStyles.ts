import { createGlobalStyle } from 'styled-components';

export const GlobalStyles = createGlobalStyle`
* {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}

body {
  font-size: ${({ theme }) => theme.font.sizes.medium};
  font-family: Lato, sans-serif;
}

a {
  text-decoration: none;
}
p {
  line-height: 2em;
}
`;
